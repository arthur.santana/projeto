#!/bin/bash

run_comand() {
	comando="$1"
	start_time=$(date +%s.%N)
	mem_before=$(pmap -x $$ | tail -1 | awk '{print $4}')
	eval "$comando"
        end_time=$(date +%s.%N)
	mem_after=$(pmap -x $$ | tail -1 | awk '{print $4}')
	tempo_execucao=$(echo "escala=2; $end_time - $start_time" | bc)
	uso_memoria=$((mem_after - mem_before))
	echo "Tempo de execução: ${tempo_execucao}segundos"
	echo "Uso de memória: ${uso_memoria}KB"
	echo $tempo_execucao >> tempo_execucao.txt
}

echo "Cenário 1 : 2 Threads"
export OMP_NUM_THREADS=1
run_comand "nome do comando"

echo "Cenário 2 : 4 Threads"
export OMP_NUM_THREADS=4
run_comand "nome do comando"

echo "Cenário 3 : 8 Threads"
export OMP_NUM_THREADS=8
run_comand "nome do comando"


echo "Tempos de execução: "
echo "Cenário 1: $(sed -n '1p' tempo_execucao.txt)s"
echo "Cenário 2: $(sed -n '2p' tempo_execucao.txt)s"
echo "Cenário 3: $(sed -n '3p' tempo_execucao.txt)s"

rm tempo_execucao.txt
touch tempo_execucao.txt

